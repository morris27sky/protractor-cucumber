"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const chai_1 = require("chai");
const MortageRatesPage_1 = require("../../../Pages/Product/Mortgages/MortageRatesPage");
const HomePage_1 = require("../../../Pages/Global/Home/HomePage");
const ReadyToApplyPage_1 = require("../../../Pages/Product/Mortgages/ReadyToApplyPage");
let readyToApply = new ReadyToApplyPage_1.ReadyToApplyPage();
let mortageRates = new MortageRatesPage_1.MortageRatesPage();
var homepage = new HomePage_1.HomePage();
cucumber_1.When(/^I find mortgage rates for the following cases:$/, (table) => __awaiter(this, void 0, void 0, function* () {
    yield homepage.selectMortgageNavMenu();
    yield mortageRates.findMortgageRate(table);
}));
cucumber_1.Then(/^the results should display the following products:$/, (table) => __awaiter(this, void 0, void 0, function* () {
    const expectedValues = yield table.rows().map(row => {
        return row[0];
    });
    const actualValues = yield mortageRates.returnRatesTable();
    chai_1.expect(yield actualValues).to.deep.equal(expectedValues);
}));
cucumber_1.When(/^I apply for the '([^\"]*)' product$/, (mortgageType) => __awaiter(this, void 0, void 0, function* () {
    yield mortageRates.applyForMortgage(mortgageType);
}));
cucumber_1.When(/^I should see the heading '([^\"]*)'$/, (messageText) => __awaiter(this, void 0, void 0, function* () {
    chai_1.expect(yield readyToApply.getPageText()).to.include(messageText);
}));
//# sourceMappingURL=findMortgageRatesSteps.js.map