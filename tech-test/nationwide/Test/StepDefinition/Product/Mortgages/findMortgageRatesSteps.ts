import { When, Then, TableDefinition } from 'cucumber';
import { expect } from 'chai';
import { MortageRatesPage } from '../../../Pages/Product/Mortgages/MortageRatesPage';
import { HomePage } from '../../../Pages/Global/Home/HomePage';
import { ReadyToApplyPage } from '../../../Pages/Product/Mortgages/ReadyToApplyPage';

let readyToApply = new ReadyToApplyPage();
let mortageRates = new MortageRatesPage();
var homepage = new HomePage();

When(/^I find mortgage rates for the following cases:$/, async (table: TableDefinition) => {
    await homepage.selectMortgageNavMenu();
    await mortageRates.findMortgageRate(table);
})

Then(/^the results should display the following products:$/, async (table: TableDefinition) => {
    const expectedValues = await table.rows().map(row => {
        return row[0];
    });

    const actualValues = await mortageRates.returnRatesTable();
    expect(await actualValues).to.deep.equal(expectedValues);
})

When(/^I apply for the '([^\"]*)' product$/, async (mortgageType: String) => {
    await mortageRates.applyForMortgage(mortgageType);
})

When(/^I should see the heading '([^\"]*)'$/, async (messageText: String) => {
    expect(await readyToApply.getPageText()).to.include(messageText);
})

