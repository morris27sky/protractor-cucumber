import { Given} from 'cucumber';
import { config } from '../../../../Configuration/config';
import { browser } from 'protractor';

Given(/^I navigate to the Nationwide homepage$/, async () => {
    await browser.driver.get(config.baseUrl);
})
