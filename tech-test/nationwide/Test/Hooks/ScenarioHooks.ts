import { browser } from 'protractor';
import { After, setDefaultTimeout, Status } from 'cucumber';


setDefaultTimeout(150000);


After( async function (Scenario) {
    if (Scenario.result.status === Status.FAILED) {
        const screenShot = await browser.takeScreenshot();
        this.attach(screenShot, "image/png");
    }
});