import { by } from 'protractor';
import { BroswerExtensions } from '../../../Helpers/BrowserHelper/broswerExtentions';

var broswerActions = new BroswerExtensions();

const Locators = {
    mortgagesNavigation: by.css('#MortgagesNavItem'),
    NewCustomersNavMenuItems: by.css("[id='MortgagesNavItem'] .subNavigation .navColumnSet .menu > div:not('.service')div ul"),
    MortgageRates: by.css("[id='MortgagesNavItem'] .subNavigation .navColumnSet .menu > div ul li [data-nbs-analytics-options*='New mortgage customers|Mortgage rates']")
};

export class HomePage {

    async selectMortgageNavMenu() {
        await broswerActions.Hover(Locators.mortgagesNavigation);
        await broswerActions.WaitAndClick(Locators.MortgageRates);
    }
};