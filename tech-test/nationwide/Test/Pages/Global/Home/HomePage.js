"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
const broswerExtentions_1 = require("../../../Helpers/BrowserHelper/broswerExtentions");
var broswerActions = new broswerExtentions_1.BroswerExtensions();
const Locators = {
    mortgagesNavigation: protractor_1.by.css('#MortgagesNavItem'),
    NewCustomersNavMenuItems: protractor_1.by.css("[id='MortgagesNavItem'] .subNavigation .navColumnSet .menu > div:not('.service')div ul"),
    MortgageRates: protractor_1.by.css("[id='MortgagesNavItem'] .subNavigation .navColumnSet .menu > div ul li [data-nbs-analytics-options*='New mortgage customers|Mortgage rates']")
};
class HomePage {
    selectMortgageNavMenu() {
        return __awaiter(this, void 0, void 0, function* () {
            yield broswerActions.Hover(Locators.mortgagesNavigation);
            yield broswerActions.WaitAndClick(Locators.MortgageRates);
        });
    }
}
exports.HomePage = HomePage;
;
//# sourceMappingURL=HomePage.js.map