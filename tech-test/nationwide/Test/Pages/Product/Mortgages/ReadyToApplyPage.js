"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
const broswerExtentions_1 = require("../../../Helpers/BrowserHelper/broswerExtentions");
var broswerActions = new broswerExtentions_1.BroswerExtensions;
const Locators = {
    headerText: protractor_1.by.css("h1")
};
class ReadyToApplyPage {
    getPageText() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield broswerActions.GetInnerText(Locators.headerText);
        });
    }
}
exports.ReadyToApplyPage = ReadyToApplyPage;
;
//# sourceMappingURL=ReadyToApplyPage.js.map