"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
const broswerExtentions_1 = require("../../../Helpers/BrowserHelper/broswerExtentions");
var broswerActions = new broswerExtentions_1.BroswerExtensions;
const Locators = {
    HasNationwideMortgageQuestion: protractor_1.by.css("#HaveNationwideMortgage .selector__group div"),
    MortageTypeQuestion: protractor_1.by.css("#NationwideMortgageTypeNo .selector__group div"),
    PropertyValueField: protractor_1.by.css("#SearchPropertyValue"),
    MortgageAmountField: protractor_1.by.css("#SearchMortgageAmount"),
    MortgageTermField: protractor_1.by.css("#SearchMortgageTerm"),
    FindMortgageBtn: protractor_1.by.css(".searchButtonSection #myButton"),
    LoaderElement: protractor_1.by.css(".loading"),
    MortgageTypeCheckbox: protractor_1.by.css(".mortgageType .checkbox"),
    ProductFeeCheckbox: protractor_1.by.css(".feeOrNoFee .checkbox"),
    MortgageRateTable: protractor_1.by.css("#NewMortgageRateTables table"),
    MortgageRateTableProductHeading: protractor_1.by.css("#NewMortgageRateTables table .notOnMobile.productHeading"),
    MoreInfo: protractor_1.by.css("#NewMortgageRateTables .ratesTableWrapper._5yr .toggleMoreDetails.desktop"),
    ApplyBtn: protractor_1.by.css("#NewMortgageRateTables .ratesTableWrapper._5yr .applyButton")
};
class MortageRatesPage {
    findMortgageRate(table) {
        return __awaiter(this, void 0, void 0, function* () {
            const expectedValues = yield table.rowsHash();
            yield this.selectOption(Locators.HasNationwideMortgageQuestion, expectedValues.have_nationwide_mortgage);
            yield this.selectOption(Locators.MortageTypeQuestion, expectedValues.type_of_mortgage_question);
            yield broswerActions.WaitAndSendKeys(Locators.PropertyValueField, expectedValues.property_value);
            yield broswerActions.WaitAndSendKeys(Locators.MortgageAmountField, expectedValues.mortgage_amount);
            yield broswerActions.WaitAndSendKeys(Locators.MortgageTermField, expectedValues.mortgage_term);
            yield broswerActions.WaitAndClick(Locators.FindMortgageBtn);
            yield broswerActions.WaitUntilIsInvisible(Locators.LoaderElement);
            yield this.selectOption(Locators.MortgageTypeCheckbox, expectedValues.mortgage_type_checkbox);
            yield broswerActions.WaitUntilIsInvisible(Locators.LoaderElement);
            yield this.selectOption(Locators.ProductFeeCheckbox, expectedValues.product_fee_checkbox);
            yield broswerActions.WaitUntilIsInvisible(Locators.LoaderElement);
            yield broswerActions.WaitForElementToBeVisible(Locators.MortgageRateTable);
        });
    }
    selectOption(Element, ValueToSelect) {
        return __awaiter(this, void 0, void 0, function* () {
            yield broswerActions.WaitForElementToBeVisible(Element);
            yield protractor_1.element.all(Element).filter(function (elem) {
                return elem.getText().then(function (text) {
                    return text === ValueToSelect;
                });
            }).first().click();
        });
    }
    returnRatesTable() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield broswerActions.GetAllInnerText(Locators.MortgageRateTableProductHeading);
        });
    }
    applyForMortgage(value) {
        return __awaiter(this, void 0, void 0, function* () {
            switch (value) {
                case "5 yr Fixed":
                    yield broswerActions.WaitAndClick(Locators.MoreInfo);
                    yield broswerActions.WaitAndClick(Locators.ApplyBtn);
                    break;
                default:
                    break;
            }
        });
    }
}
exports.MortageRatesPage = MortageRatesPage;
;
//# sourceMappingURL=MortageRatesPage.js.map