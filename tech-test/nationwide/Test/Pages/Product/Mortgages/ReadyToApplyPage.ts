import { by } from 'protractor';
import { BroswerExtensions } from '../../../Helpers/BrowserHelper/broswerExtentions';


var broswerActions = new BroswerExtensions;

const Locators = {
    headerText: by.css("h1")
};

export class ReadyToApplyPage {

    async getPageText() {
      return await broswerActions.GetInnerText(Locators.headerText);
    }
};