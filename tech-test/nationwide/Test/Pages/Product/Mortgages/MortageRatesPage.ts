import { by, element } from 'protractor';
import { BroswerExtensions } from '../../../Helpers/BrowserHelper/broswerExtentions';
import { By } from 'selenium-webdriver';


var broswerActions = new BroswerExtensions;

const Locators = {
    HasNationwideMortgageQuestion: by.css("#HaveNationwideMortgage .selector__group div"),
    MortageTypeQuestion: by.css("#NationwideMortgageTypeNo .selector__group div"),
    PropertyValueField: by.css("#SearchPropertyValue"),
    MortgageAmountField: by.css("#SearchMortgageAmount"),
    MortgageTermField: by.css("#SearchMortgageTerm"),
    FindMortgageBtn: by.css(".searchButtonSection #myButton"),
    LoaderElement: by.css(".loading"),
    MortgageTypeCheckbox: by.css(".mortgageType .checkbox"),
    ProductFeeCheckbox: by.css(".feeOrNoFee .checkbox"),
    MortgageRateTable: by.css("#NewMortgageRateTables table"),
    MortgageRateTableProductHeading: by.css("#NewMortgageRateTables table .notOnMobile.productHeading"),
    MoreInfo: by.css("#NewMortgageRateTables .ratesTableWrapper._5yr .toggleMoreDetails.desktop"),
    ApplyBtn: by.css("#NewMortgageRateTables .ratesTableWrapper._5yr .applyButton")
};

export class MortageRatesPage {

    async findMortgageRate(table) {
        const expectedValues = await table.rowsHash();

        await this.selectOption(Locators.HasNationwideMortgageQuestion, expectedValues.have_nationwide_mortgage); 
        await this.selectOption(Locators.MortageTypeQuestion, expectedValues.type_of_mortgage_question); 
        await broswerActions.WaitAndSendKeys(Locators.PropertyValueField, expectedValues.property_value); 
        await broswerActions.WaitAndSendKeys(Locators.MortgageAmountField, expectedValues.mortgage_amount); 
        await broswerActions.WaitAndSendKeys(Locators.MortgageTermField, expectedValues.mortgage_term);
        await broswerActions.WaitAndClick(Locators.FindMortgageBtn);
        await broswerActions.WaitUntilIsInvisible(Locators.LoaderElement);
        await this.selectOption(Locators.MortgageTypeCheckbox, expectedValues.mortgage_type_checkbox);
        await broswerActions.WaitUntilIsInvisible(Locators.LoaderElement);
        await this.selectOption(Locators.ProductFeeCheckbox, expectedValues.product_fee_checkbox);
        await broswerActions.WaitUntilIsInvisible(Locators.LoaderElement);
        await broswerActions.WaitForElementToBeVisible(Locators.MortgageRateTable);
        
    }

    async selectOption(Element: By, ValueToSelect: String){
        await broswerActions.WaitForElementToBeVisible(Element)
        await element.all(Element).filter(function(elem) {
            return elem.getText().then(function(text) {
                return text === ValueToSelect;
            });
        }).first().click();
    }

    async returnRatesTable() {
        return await broswerActions.GetAllInnerText(Locators.MortgageRateTableProductHeading);
    }

    async applyForMortgage(value) {
        switch (value) {
            case "5 yr Fixed":
                await broswerActions.WaitAndClick(Locators.MoreInfo);
                await broswerActions.WaitAndClick(Locators.ApplyBtn);
                break;
            default:
                break;
        }
    }

};