import { protractor, browser, element, $ } from "protractor";
import { By } from "selenium-webdriver";
import { config } from "../../../Configuration/config";

var ec = protractor.ExpectedConditions;

export class BroswerExtensions {

    async WaitForElementToBeVisible(webElementToWaitFor: By) {

        await browser.sleep(config.bronzeServerDelayWaitTimeout);
        var elm = await element(webElementToWaitFor);
        await browser.wait(ec.visibilityOf(elm), config.browserWaitTimeout, webElementToWaitFor + ": was not visible after " + config.browserWaitTimeout +" millisecond");
        return elm;
    }

    async WaitUntilIsInvisible(webElementToWaitFor: By) {

        await browser.sleep(config.bronzeServerDelayWaitTimeout);
        var elm = await element(webElementToWaitFor);
        await browser.wait(ec.not(ec.visibilityOf(elm)), config.browserWaitTimeout, webElementToWaitFor + ": was not visible after " + config.browserWaitTimeout +" millisecond");
        return elm;
    }

    async WaitForElementsToBeVisible(webElementToWaitFor: By, index: number) {

        await browser.sleep(config.bronzeServerDelayWaitTimeout);
        var elm = await element.all(webElementToWaitFor);
        await browser.wait(ec.visibilityOf(elm[index]), config.browserWaitTimeout, webElementToWaitFor + ": was not visible after " + config.browserWaitTimeout +" millisecond");
        return elm;
    }
    
    async WaitForElementToBeClickble(webElementToWaitFor: By) {

        await browser.sleep(config.bronzeServerDelayWaitTimeout);
        var elm = await element(webElementToWaitFor);
        await browser.wait(ec.elementToBeClickable(elm), config.browserWaitTimeout, webElementToWaitFor + ": was not visible after " + config.browserWaitTimeout +" millisecond");
        return elm;
    }

    async WaitAndClick(webElementToClick: By) {

        var elm = await this.WaitForElementToBeVisible(webElementToClick);
        return await elm.click();
    }

    async WaitAndClickByIndex(webElementToClick: By, index: number) {

        await this.WaitForElementToBeClickble(webElementToClick);
        var elm = await element.all(webElementToClick);
        await elm[index].click();
    }

    async WaitAndSendKeys(webElementToSendKeys: By, text: string) {

        var elm = await this.WaitForElementToBeVisible(webElementToSendKeys);
        return await elm.sendKeys(text);
    }

    async GetInnerText(webElementToExtractInnerText: By) {

        var elm = await this.WaitForElementToBeVisible(webElementToExtractInnerText);
        return await elm.getText();
    }

    async GetAllInnerText(webElementToExtractInnerText: By) {

        await this.WaitForElementToBeVisible(webElementToExtractInnerText);
        return await element.all(webElementToExtractInnerText).map(function (element) {
            return element.getText()
        });
    }

    async Hover(elementToHoverOver: By) {

        await this.WaitForElementToBeVisible(elementToHoverOver);
        var elm = await element(elementToHoverOver);
        await browser.actions().mouseMove(elm).perform();
    }

    async IsElementDisplayed(elementToBeDisplayed: By) {

        var ele = await this.WaitForElementToBeVisible(elementToBeDisplayed);
        return await ele.isDisplayed();
    }

    async IsElementPresent(elementToCheckPresence: By){

        return await element(elementToCheckPresence).isPresent();
    }
}