@mortgages
Feature: Find a mortgage rate
  As a new customer
  I want to find the mortgage rates available
  So that I can decide whether to switch my mortgage to Nationwide

  Scenario: Find mortgage rates
    Given I navigate to the Nationwide homepage
    When I find mortgage rates for the following cases:
      | have_nationwide_mortgage  | No                  |
      | type_of_mortgage_question | I'm changing lender |
      | property_value            | 300000              |
      | mortgage_amount           | 150000              |
      | mortgage_term             | 30                  |
      | mortgage_type_checkbox    | Fixed rate          |
      | product_fee_checkbox      | With fee            |
    Then the results should display the following products:
      | Heading     |
      | 2 yr Fixed  |
      | 3 yr Fixed  |
      | 5 yr Fixed  |
      | 10 yr Fixed |
    When I apply for the '5 yr Fixed' product
    Then I should see the heading 'Start your remortgage application'

## Quick start
# run `npm start`

## Notes
# The project is setup with babel to support ES6 / ES2016.
# Feel free to update and change the setup & configs as you require.
