Setup
============
```sh
$ cd interview/tech-test/nationwide
$ npm i
$ npm i -g protractor
$ npm i -g typescript
$ npm webdriver-manager update (might need sudo)
```

npm webdriver-manager update (might need sudo)

To run test
============
```sh
$ npm test
$  or cd to configuration folder and run
$  protractor config.js to run all @mortgages tests
```

Notes
========
Could easily assert other things on the page but due to lack of time, prefer to go over this if selected for the interview. I only followeed what was asked of me as per the readme in the zip